
<?php 
require_once("messages.php");
class MessagesTest extends PHPUnit_Framework_TestCase{
	public function setUp(){
		//echo " I run before each test \n";
	}	

	public function testAjouterMessageFichierInexistant(){
		echo " on ajoute un message dans fichier inexistant\n";
		
		ajouterMessage("John","Hello world","testmessages.json","11");

		$this->assertFileEquals('./expected1_messages.json', './testmessages.json');
		
	}
	public function testAjouterMessageFichierexistant(){
		echo " on ajoute un message dans fichier deja existant\n";

		//On creer le fichier existant (donc avec une valeur presente)
		$file = fopen ("testmessages.json", "w");	
		$data["messages"] = array( array('pseudo' =>"John", 'time'=>"11", 'message'=>"Hello world"));
		fwrite($file, json_encode($data));
		fclose($file);

		//on ajoute le pseudo
		ajouterMessage("Doe","Hello world","testmessages.json","11");
		//on verifie qu on obtient le fichier attendu
		$this->assertFileEquals('./expected2_messages.json', './testmessages.json');
		
	}

	public function tearDown(){
		//echo" I run after each test \n";
		unlink("testmessages.json");

	}
}
?>