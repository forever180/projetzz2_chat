<?php

	if(empty($_SESSION['lang']))
		$_SESSION['lang']="FR"; // par defaut on utilise le francais

	if (isset($_GET['lang']))
		checkLanguage($_GET['lang']); // on vérifie qu'on a pas une langue en parametre GET et qu'il faille donc changer le parametre de session

	function checkLanguage($lang)
	{
		if(!empty($lang)){

			$lang = htmlentities($lang);
			$filename="traduction.json";
		
			if (file_exists($filename)) {
				$json = json_decode(file_get_contents($filename),true);
				if (array_key_exists($lang, $json)) // Verification d'une langue valide
				{
   					$_SESSION['lang']=$lang;
				}
			}
		}			
	}
	

// Fonction de traduction à partir d'une clé d'index, la langue est définis par un paramètre de session
	function trad($cle){

		$filename="traduction.json";
		$resultat=$cle;

		if (file_exists($filename)) {
			$json = json_decode(file_get_contents($filename),true);
			$lang=$_SESSION['lang'];
			
			if (array_key_exists($cle, $json[$lang]))
			{
   				$resultat=$json[$lang][$cle];
			}
		}
		return $resultat;
	}
	
?>