
<?php 
require_once("traduction.php");
class TraductionTest extends PHPUnit_Framework_TestCase{
	public function setUp(){
		//echo " I run before each test \n";
	}	

	public function testTraduireCleExistante(){

		echo " on traduit une cle existante \n";	

		$this->assertEquals(trad("title"),"Mon Super Chat"); 
	}

	public function testTraduireCleInexistante(){

		echo " on traduit une cle inexistante \n";	

		$this->assertEquals(trad("title2"),"title2"); 
	}

	public function testChecklangageCorect(){

		echo " On test si en chargeant une langue corect on obtient le bon parametre de session\n";	
		checkLanguage("EN");
		$this->assertEquals($_SESSION['lang'],"EN"); 
	}

	public function testChecklangageIncorect(){

		echo " On test si en chargeant un langue incorect on obtient le bon parametre de session (ie celui precedent)\n";	
		$langprecedente=$_SESSION['lang'];
		checkLanguage("EN3");
		$this->assertEquals($_SESSION['lang'],$langprecedente); 
	}

	public function tearDown(){
		//echo" I run after each test \n";
	}
}

?>