<?php
session_start();

function ajouterMessage ($pseudo,$message,$filename,$date)
{
	if (file_exists($filename)){
		//Si le fichier existe on a juste a push le pseudo et le message
		$json = json_decode(file_get_contents($filename),true);
		array_push($json['messages'], array('pseudo' => $pseudo, 'time'=>$date, 'message'=>$message));		
		file_put_contents($filename, json_encode($json));
	}
	else{
		//creer le fichier et on initialise

		$file = fopen($filename, 'w');		
		$data["messages"] = array( array('pseudo' => $pseudo, 'time'=>$date, 'message'=>$message));
		fwrite($file, json_encode($data));
		fclose($file);
	}
}

$message=trim(htmlentities($_POST['message']));  // traitement securite

if ( isset($message) && !empty($message) ) {

	$filename="messages.json";
	$pseudo=$_SESSION['pseudo'];
	
	ajouterMessage($pseudo,$message,$filename,date("H:i:s"));	
}
	


?>