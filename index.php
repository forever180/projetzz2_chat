<?php session_start(); ?>
<?php include("traduction.php"); ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css" >
        <link rel="stylesheet" href="css/bootstrap-markdown.min.css">
        <script src="js/jquery-1.11.1.min.js"></script>
        <script src="js/bootstrap.min.js "></script>
        <script src="js/bootstrap-markdown.js"></script>
        <script src="js/markdown.js"></script> 

        <title><?php echo trad("title"); ?></title>


    </head>

    <body>

    <div class="container">
        <h1><?php echo trad("title"); ?></h1>
        
        <ul class="nav nav-pills" role="tablist">
            <li class="active"><a href="#home" data-toggle="tab">Home</a></li>
            <li><a href="index.php?lang=<?php if($_SESSION['lang']=="FR"){echo "EN";} else{echo "FR";} ?> ">Version <?php if ($_SESSION['lang']=="FR") { echo "Eng";}else{echo "Fr";} ?></a></li>
            <li><a href="deconnexion.php" ><?php echo trad("logout"); ?></a></li>
        </ul>
        
        <div class="tab-content">

            <div class="tab-pane active fade in" id="home">
                
            <?php if ( empty($_SESSION['pseudo']) && !isset ($_SESSION['pseudo']) ) { ?> 
            <div class="well">

                <form role="form" action="connexion.php" method="post">

                    <div class="form-group">
                        <label for="pseudo">Pseudo </label>
                        <?php if (isset($_SESSION ['erreur'])){ ?>
                        <div class="alert alert-danger" role="alert"><?php echo $_SESSION['erreur']; ?></div> <?php } ?>
                        <input type="text" class="form-control" id="pseudo" placeholder="Entrer un Pseudo" name="pseudo" <?php if (isset($_COOKIE["Connexion"])) { ?> value=<?php  echo $_COOKIE["Connexion"]; } ?>  >
                        
                    </div>

                    <div class="checkbox">
                        <label>
                          <input type="checkbox" name="remember" <?php if (isset($_COOKIE["Connexion"])) { ?> checked="checked" <?php } ?> ><?php echo trad("Remember") ?></input>
                        </label>
                    </div>

                    <button type="submit" class="btn btn-default">Envoyer</button>

                </form>

            </div>    

            <?php } else { ?>
                    
                <div class="col-md-9">

                    <h2> <?php echo trad("messages"); ?> - <?php echo $_SESSION['pseudo'];?></h2>
                    <p class="well message" id="messages">
                    </p>

                </div>

                <div class="col-md-3">

                    <h2><?php echo trad("connected"); ?></h2>
                    <p class="well message" id="pseudos"> 
                      
                    </p>

                </div>

                <div class="col-md-9">
                    <form role="form" method="post" id="formulaire">
                        <div class="well input-group">
                            <textarea type="text" class="form-control input-sm" placeholder="Votre message" name="message" id="message" data-provide="markdown" data-hidden-buttons="cmdHeading"></textarea>
                            <span class="input-group-btn ">
                                 <button class="btn btn-default input-sm" type="submit"><?php echo trad("send"); ?></button>
                            </span>
                        </div>
                    </form>
                  
                   
                </div>

            <?php }; ?>

            </div>


           
        

    </div>

    </body>
<?php if ( !empty($_SESSION['pseudo']) && isset ($_SESSION['pseudo']) ) { ?> 
   <script type="text/javascript"> 
  

         function requetePseudos(){
            
            $.ajax({
                url : "chargerPseudos.php",
                type : "GET",
                success : function(html){
                    
                    // document.getElementById('message').innerHTML = html;
                    $( '#pseudos' ).html( html ); // on utilise jQuery 
                }
            });

        };

        requetePseudos();
        setInterval( function() {requetePseudos();}, 5000); // on exécute le chargement toutes les 5 secondes



         function requeteMessages(){
            
            $.ajax({
                url : "chargerMessages.php",
                type : "GET",
                success : function(html){
                    
                    // document.getElementById('message').innerHTML = html;
                    $( '#messages' ).html(html); // on utilise jQuery 
                }
            });

        };
        requeteMessages();
        setInterval( function() {requeteMessages();}, 5000); // on execute le chargement toutes les 5 secondes
        
        $(document).ready(function(){
            $("#formulaire").submit(function(){
                $.ajax({
                    type:"POST",
                    data: $(this).serialize(),
                    url:"messages.php", 
                    success : function(html){
                    $( '#message' ).val(''); 
                    requeteMessages();
                    }
                });
                return false;
            });
        });

    </script>
    <?php } ?>

</html>