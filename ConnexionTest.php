<?php 
require_once("connexion.php");
class TestConnexion extends PHPUnit_Framework_TestCase{
	public function setUp(){
		//echo " I run before each test \n";
	}

	public function testRechercheTrouveSimple(){
		echo " on a trouve le mot dans le fichier de 1 mot \n";

		

		$file = fopen ("testconnecte.json", "w");
		$data["pseudos"] = array( array('pseudo' => "maxime"));
		fwrite($file, json_encode($data));
		fclose($file);

		$this->assertTrue(recherche_pseudo("maxime","testconnecte.json"));
				
	}

	public function testRechercheTrouveDeuxMots(){
		echo " on a trouve le mot dans le fichier de 2 mots \n";

		$file = fopen ("testconnecte.json", "w");
		$data["pseudos"] = array( array('pseudo' => "maxime2"),array('pseudo' => "maxime"));
		fwrite($file, json_encode($data));
		fclose($file);


		$this->assertTrue(recherche_pseudo("maxime","testconnecte.json"));
		
		
	}
	
	public function testAjouterFichierInexistant(){
		echo " on ajoute un pseudo dans fichier inexistant\n";
		ajouterpseudo("John","testconnecte.json");

		$this->assertFileExists("testconnecte.json");
		$this->assertFileEquals('./expected1.json', './testconnecte.json');
		
	}
	public function testAjouterFichierexistant(){
		echo " on ajoute un pseudo dans fichier deja existant\n";

		//On creer le fichier existant (donc avec une valeur presente)
		$file = fopen ("testconnecte.json", "w");	
		$data["pseudos"] = array( array('pseudo' => "john Doe"));
		fwrite($file, json_encode($data));
		fclose($file);

		//on ajoute le pseudo
		ajouterpseudo("John","testconnecte.json");
		//on verifie qu on obtient le fichier attendu
		$this->assertFileEquals('./expected2.json', './testconnecte.json');
		
	}

	public function tearDown(){
		//echo" I run after each test \n";
		unlink("testconnecte.json");

	}
}
?>