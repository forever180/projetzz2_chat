<?php

session_start();

function recherche_pseudo($pseudo,$filename){

	//initialisation
	$trouve=false;
	if (file_exists($filename)) {

		$json = json_decode(file_get_contents($filename)); // on est oblige de recuperer tout le fichier
			
		foreach( $json->pseudos as $item ){ // On definit item comme un element de pseudos

			if( $item->pseudo == $pseudo ){ // On regarde si l'attribut pseudo de item corespond				{
				$trouve=true;
			}
		} // pas optimiser ... un while suffit
	}

    return $trouve;

}

function ajouterPseudo ($pseudo,$filename)
{
	if (file_exists($filename)){
		//Si le fichier existe on a juste a push le pseudo
		$json = json_decode(file_get_contents($filename),true);
		array_push($json['pseudos'], array('pseudo' => $pseudo));		
		file_put_contents($filename, json_encode($json));
	}
	else{
		//creer le fichier et on initialise

		$file = fopen($filename, 'w');		
		$data["pseudos"] = array( array('pseudo' => $pseudo));
		fwrite($file, json_encode($data));
		fclose($file);
	}
}


if ( isset($_POST['pseudo']) && !empty($_POST['pseudo']) ) {

	$filename="connectes.json";
	$pseudo=trim(htmlentities($_POST['pseudo'])); // traitement securite
	
	if (!recherche_pseudo($pseudo,$filename)) { 
	//Si on ne trouve pas, on se connecte
		$_SESSION['pseudo']=$pseudo;
		unset($_SESSION['erreur']);
		// et on ajoute le pseudo dans le fichier des connectes
		ajouterPseudo($pseudo,$filename);
		
		// Si on veut se rappeler de l utilisateur on creer un cookie
		if (isset($_POST['remember']) ) {
			// set cookies
			setcookie("Connexion", $pseudo, time()+3600); 
		}

		
	}
	else{
		$_SESSION['erreur']="Pseudo déjà utilisé";
	}
	
} 
header('Location: index.php');
?>