<?php
session_start ();

function effacerPseudo($pseudo,$filename){
	
	if (file_exists($filename)) {
		
		$json = json_decode(file_get_contents($filename),true);
		foreach ($json['pseudos'] as $i => $item) {
			if ($item['pseudo'] == $pseudo) {
				unset($json['pseudos'][$i]); 
			}	
		} // supprime aussi si 2 apparitions (pas optimiser...)
		file_put_contents($filename, json_encode($json));
	}	
} 

effacerPseudo($_SESSION['pseudo'],"connectes.json");

session_unset();
session_destroy();

header('Location: index.php');



?>