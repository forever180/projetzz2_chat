
<?php 
require_once("deconnexion.php");
class TestDeconnexion extends PHPUnit_Framework_TestCase{
	public function setUp(){
		//echo " I run before each test \n";
	}

	

	public function testEffacementPseudo(){

		echo " on efface le pseudo dans un fichier \n";	

		$file = fopen ("testdeconnecte.json", "w");
		$data["pseudos"] = array( array('pseudo' => "maxime1"),array('pseudo' => "maxime2"),array('pseudo' => "maxime3"));
		fwrite($file, json_encode($data));
		fclose($file);
		effacerPseudo("maxime2","testdeconnecte.json");

		$this->assertFileEquals('./expected1_pseudo.json', './testdeconnecte.json');
		
	}


	public function testEffacementPseudoInexistant(){

		echo " on essaye d effacer un pseudo inexistant dans le fichier \n";	

		$file = fopen ("testdeconnecte.json", "w");
		$data["pseudos"] = array( array('pseudo' => "maxime1"),array('pseudo' => "maxime2"),array('pseudo' => "maxime3"));
		fwrite($file, json_encode($data));
		fclose($file);

		effacerPseudo("maxime","testdeconnecte.json");
		$this->assertFileEquals('./expected2_pseudo.json', './testdeconnecte.json');
	}
	

	public function tearDown(){
		//echo" I run after each test \n";
	}
}